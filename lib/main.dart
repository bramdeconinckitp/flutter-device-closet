import 'package:flutter/material.dart';

import 'app.dart';
import 'core/services/injection.dart';
void main() {
  WidgetsFlutterBinding.ensureInitialized();
  initDependencyInjection();
  runApp(App());
}
