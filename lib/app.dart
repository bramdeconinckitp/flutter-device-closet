import 'package:flutter/material.dart';
import 'package:flutter_device_closet/core/services/navigation/router/navigation_router.dart';

import 'core/services/dialogs/dialog_manager.dart';
import 'core/services/injection.dart';
import 'core/services/navigation/service/navigation_service.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      navigatorKey: getIt<NavigationService>().navigatorKey,
      onGenerateRoute: getIt<NavigationRouter>().generateRoute,
      initialRoute: NavigationRouter.devices,
      builder: (context, widget) {
        return Navigator(onGenerateRoute: (settings) {
          return MaterialPageRoute<dynamic>(builder: (context) {
            return DialogManager(child: widget);
          });
        });
      },
    );
  }
}
