import 'package:flutter_device_closet/core/api/device_closet_api.dart';
import 'package:flutter_device_closet/features/devices/business/entities/device.dart';
import 'package:flutter_device_closet/features/devices/business/repositories/device_repository.dart';
import 'package:flutter_device_closet/features/devices/data/mappers/device_model_to_device_mapper.dart';
import 'package:flutter_device_closet/features/devices/data/models/check_device_out_body_model.dart';
import 'package:flutter_device_closet/features/devices/data/models/device_model.dart';
import 'package:flutter_device_closet/features/users/business/entities/user.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@RegisterAs(DeviceRepository)
@lazySingleton
class DeviceRepositoryImpl implements DeviceRepository {
  static const String checkDeviceInOutSuccessStatus = 'success';

  final DeviceClosetApi _deviceClosetApi;
  final DeviceModelToDeviceMapper _deviceModelToDeviceMapper;

  DeviceRepositoryImpl(
    this._deviceClosetApi,
    this._deviceModelToDeviceMapper,
  );

  @override
  Observable<List<Device>> getAllDevices() {
    return Observable.fromFuture(_getDevicesFromApi()).map(_mapDeviceModelsToDevices);
  }

  @override
  Observable<Device> getDeviceById(String deviceId) {
    return Observable.fromFuture(_getDeviceByIdFromApi(deviceId)).map(_mapDeviceModelToDevice);
  }

  @override
  Future<bool> checkDeviceOut(Device device, User user) {
    // TODO: Post body instead of using toJson().
    final CheckDeviceOutBodyModel body = CheckDeviceOutBodyModel(userId: user.id);

    return _deviceClosetApi
        .checkDeviceOut(device.id, body.toJson())
        .then((response) => response.body.status == checkDeviceInOutSuccessStatus);
  }

  @override
  Future<bool> checkDeviceIn(Device device) {
    // TODO: Post body instead of using toJson().
    return _deviceClosetApi
        .checkDeviceIn(device.id)
        .then((response) => response.body.status == checkDeviceInOutSuccessStatus);
  }

  Future<List<DeviceModel>> _getDevicesFromApi() async {
    return _deviceClosetApi.getDevices().then((res) => res.body);
  }

  Future<DeviceModel> _getDeviceByIdFromApi(String deviceId) {
    return _deviceClosetApi.getDeviceById(deviceId).then((response) => response.body);
  }

  List<Device> _mapDeviceModelsToDevices(List<DeviceModel> deviceModels) {
    return deviceModels.map(_mapDeviceModelToDevice).toList();
  }

  Device _mapDeviceModelToDevice(DeviceModel deviceModel) {
    return _deviceModelToDeviceMapper.to(deviceModel);
  }
}
