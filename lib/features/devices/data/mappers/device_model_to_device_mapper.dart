import 'package:flutter_device_closet/features/devices/business/entities/device.dart';
import 'package:flutter_device_closet/features/devices/business/entities/format.dart';
import 'package:flutter_device_closet/features/devices/data/models/device_model.dart';
import 'package:flutter_device_closet/features/users/business/entities/user.dart';
import 'package:flutter_device_closet/features/users/data/mappers/user_model_to_user_mapper.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class DeviceModelToDeviceMapper {
  final UserModelToUserMapper _userModelToUserMapper;

  DeviceModelToDeviceMapper(this._userModelToUserMapper);

  Device to(DeviceModel model) {
    final User user = model.assignedTo == null ? null : _userModelToUserMapper.to(model.assignedTo);

    return Device(
      id: model.id,
      tag: model.assetTag,
      brand: model.manufacturer?.name,
      format: parseFormat(model.category?.name),
      imageUrl: model.image,
      location: model.rtdLocation?.name,
      isAvailable: model.userCanCheckout,
      model: model.model?.name,
      assignee: user,
    );
  }
}
