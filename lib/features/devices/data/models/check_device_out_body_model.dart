import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'check_device_out_body_model.freezed.dart';
part 'check_device_out_body_model.g.dart';

@freezed
abstract class CheckDeviceOutBodyModel with _$CheckDeviceOutBodyModel {
  factory CheckDeviceOutBodyModel({
    @JsonKey(name: 'userId') int userId,
  }) = _CheckDeviceOutBodyModel;

  factory CheckDeviceOutBodyModel.fromJson(Map<String, dynamic> json) => _$CheckDeviceOutBodyModelFromJson(json);
}
