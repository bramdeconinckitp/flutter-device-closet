// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'model_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
ModelModel _$ModelModelFromJson(Map<String, dynamic> json) {
  return _ModelModel.fromJson(json);
}

class _$ModelModelTearOff {
  const _$ModelModelTearOff();

  _ModelModel call(
      {@JsonKey(name: 'id') int id, @JsonKey(name: 'name') String name}) {
    return _ModelModel(
      id: id,
      name: name,
    );
  }
}

// ignore: unused_element
const $ModelModel = _$ModelModelTearOff();

mixin _$ModelModel {
  @JsonKey(name: 'id')
  int get id;
  @JsonKey(name: 'name')
  String get name;

  Map<String, dynamic> toJson();
  $ModelModelCopyWith<ModelModel> get copyWith;
}

abstract class $ModelModelCopyWith<$Res> {
  factory $ModelModelCopyWith(
          ModelModel value, $Res Function(ModelModel) then) =
      _$ModelModelCopyWithImpl<$Res>;
  $Res call({@JsonKey(name: 'id') int id, @JsonKey(name: 'name') String name});
}

class _$ModelModelCopyWithImpl<$Res> implements $ModelModelCopyWith<$Res> {
  _$ModelModelCopyWithImpl(this._value, this._then);

  final ModelModel _value;
  // ignore: unused_field
  final $Res Function(ModelModel) _then;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as int,
      name: name == freezed ? _value.name : name as String,
    ));
  }
}

abstract class _$ModelModelCopyWith<$Res> implements $ModelModelCopyWith<$Res> {
  factory _$ModelModelCopyWith(
          _ModelModel value, $Res Function(_ModelModel) then) =
      __$ModelModelCopyWithImpl<$Res>;
  @override
  $Res call({@JsonKey(name: 'id') int id, @JsonKey(name: 'name') String name});
}

class __$ModelModelCopyWithImpl<$Res> extends _$ModelModelCopyWithImpl<$Res>
    implements _$ModelModelCopyWith<$Res> {
  __$ModelModelCopyWithImpl(
      _ModelModel _value, $Res Function(_ModelModel) _then)
      : super(_value, (v) => _then(v as _ModelModel));

  @override
  _ModelModel get _value => super._value as _ModelModel;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
  }) {
    return _then(_ModelModel(
      id: id == freezed ? _value.id : id as int,
      name: name == freezed ? _value.name : name as String,
    ));
  }
}

@JsonSerializable()
class _$_ModelModel with DiagnosticableTreeMixin implements _ModelModel {
  _$_ModelModel(
      {@JsonKey(name: 'id') this.id, @JsonKey(name: 'name') this.name});

  factory _$_ModelModel.fromJson(Map<String, dynamic> json) =>
      _$_$_ModelModelFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'name')
  final String name;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ModelModel(id: $id, name: $name)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ModelModel'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('name', name));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ModelModel &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name);

  @override
  _$ModelModelCopyWith<_ModelModel> get copyWith =>
      __$ModelModelCopyWithImpl<_ModelModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ModelModelToJson(this);
  }
}

abstract class _ModelModel implements ModelModel {
  factory _ModelModel(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String name}) = _$_ModelModel;

  factory _ModelModel.fromJson(Map<String, dynamic> json) =
      _$_ModelModel.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id;
  @override
  @JsonKey(name: 'name')
  String get name;
  @override
  _$ModelModelCopyWith<_ModelModel> get copyWith;
}
