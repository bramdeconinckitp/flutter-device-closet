// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'rtd_location_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
RtdLocationModel _$RtdLocationModelFromJson(Map<String, dynamic> json) {
  return _RtdLocationModel.fromJson(json);
}

class _$RtdLocationModelTearOff {
  const _$RtdLocationModelTearOff();

  _RtdLocationModel call(
      {@JsonKey(name: 'id') int id, @JsonKey(name: 'name') String name}) {
    return _RtdLocationModel(
      id: id,
      name: name,
    );
  }
}

// ignore: unused_element
const $RtdLocationModel = _$RtdLocationModelTearOff();

mixin _$RtdLocationModel {
  @JsonKey(name: 'id')
  int get id;
  @JsonKey(name: 'name')
  String get name;

  Map<String, dynamic> toJson();
  $RtdLocationModelCopyWith<RtdLocationModel> get copyWith;
}

abstract class $RtdLocationModelCopyWith<$Res> {
  factory $RtdLocationModelCopyWith(
          RtdLocationModel value, $Res Function(RtdLocationModel) then) =
      _$RtdLocationModelCopyWithImpl<$Res>;
  $Res call({@JsonKey(name: 'id') int id, @JsonKey(name: 'name') String name});
}

class _$RtdLocationModelCopyWithImpl<$Res>
    implements $RtdLocationModelCopyWith<$Res> {
  _$RtdLocationModelCopyWithImpl(this._value, this._then);

  final RtdLocationModel _value;
  // ignore: unused_field
  final $Res Function(RtdLocationModel) _then;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as int,
      name: name == freezed ? _value.name : name as String,
    ));
  }
}

abstract class _$RtdLocationModelCopyWith<$Res>
    implements $RtdLocationModelCopyWith<$Res> {
  factory _$RtdLocationModelCopyWith(
          _RtdLocationModel value, $Res Function(_RtdLocationModel) then) =
      __$RtdLocationModelCopyWithImpl<$Res>;
  @override
  $Res call({@JsonKey(name: 'id') int id, @JsonKey(name: 'name') String name});
}

class __$RtdLocationModelCopyWithImpl<$Res>
    extends _$RtdLocationModelCopyWithImpl<$Res>
    implements _$RtdLocationModelCopyWith<$Res> {
  __$RtdLocationModelCopyWithImpl(
      _RtdLocationModel _value, $Res Function(_RtdLocationModel) _then)
      : super(_value, (v) => _then(v as _RtdLocationModel));

  @override
  _RtdLocationModel get _value => super._value as _RtdLocationModel;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
  }) {
    return _then(_RtdLocationModel(
      id: id == freezed ? _value.id : id as int,
      name: name == freezed ? _value.name : name as String,
    ));
  }
}

@JsonSerializable()
class _$_RtdLocationModel
    with DiagnosticableTreeMixin
    implements _RtdLocationModel {
  _$_RtdLocationModel(
      {@JsonKey(name: 'id') this.id, @JsonKey(name: 'name') this.name});

  factory _$_RtdLocationModel.fromJson(Map<String, dynamic> json) =>
      _$_$_RtdLocationModelFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'name')
  final String name;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RtdLocationModel(id: $id, name: $name)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'RtdLocationModel'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('name', name));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RtdLocationModel &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name);

  @override
  _$RtdLocationModelCopyWith<_RtdLocationModel> get copyWith =>
      __$RtdLocationModelCopyWithImpl<_RtdLocationModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_RtdLocationModelToJson(this);
  }
}

abstract class _RtdLocationModel implements RtdLocationModel {
  factory _RtdLocationModel(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String name}) = _$_RtdLocationModel;

  factory _RtdLocationModel.fromJson(Map<String, dynamic> json) =
      _$_RtdLocationModel.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id;
  @override
  @JsonKey(name: 'name')
  String get name;
  @override
  _$RtdLocationModelCopyWith<_RtdLocationModel> get copyWith;
}
