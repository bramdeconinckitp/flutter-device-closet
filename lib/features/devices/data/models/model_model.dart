import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'model_model.freezed.dart';
part 'model_model.g.dart';

@freezed
abstract class ModelModel with _$ModelModel {
  factory ModelModel({
    @JsonKey(name: 'id') int id,
    @JsonKey(name: 'name') String name,
  }) = _ModelModel;

    factory ModelModel.fromJson(Map<String, dynamic> json) => _$ModelModelFromJson(json);
}
