// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'check_device_out_body_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
CheckDeviceOutBodyModel _$CheckDeviceOutBodyModelFromJson(
    Map<String, dynamic> json) {
  return _CheckDeviceOutBodyModel.fromJson(json);
}

class _$CheckDeviceOutBodyModelTearOff {
  const _$CheckDeviceOutBodyModelTearOff();

  _CheckDeviceOutBodyModel call({@JsonKey(name: 'userId') int userId}) {
    return _CheckDeviceOutBodyModel(
      userId: userId,
    );
  }
}

// ignore: unused_element
const $CheckDeviceOutBodyModel = _$CheckDeviceOutBodyModelTearOff();

mixin _$CheckDeviceOutBodyModel {
  @JsonKey(name: 'userId')
  int get userId;

  Map<String, dynamic> toJson();
  $CheckDeviceOutBodyModelCopyWith<CheckDeviceOutBodyModel> get copyWith;
}

abstract class $CheckDeviceOutBodyModelCopyWith<$Res> {
  factory $CheckDeviceOutBodyModelCopyWith(CheckDeviceOutBodyModel value,
          $Res Function(CheckDeviceOutBodyModel) then) =
      _$CheckDeviceOutBodyModelCopyWithImpl<$Res>;
  $Res call({@JsonKey(name: 'userId') int userId});
}

class _$CheckDeviceOutBodyModelCopyWithImpl<$Res>
    implements $CheckDeviceOutBodyModelCopyWith<$Res> {
  _$CheckDeviceOutBodyModelCopyWithImpl(this._value, this._then);

  final CheckDeviceOutBodyModel _value;
  // ignore: unused_field
  final $Res Function(CheckDeviceOutBodyModel) _then;

  @override
  $Res call({
    Object userId = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed ? _value.userId : userId as int,
    ));
  }
}

abstract class _$CheckDeviceOutBodyModelCopyWith<$Res>
    implements $CheckDeviceOutBodyModelCopyWith<$Res> {
  factory _$CheckDeviceOutBodyModelCopyWith(_CheckDeviceOutBodyModel value,
          $Res Function(_CheckDeviceOutBodyModel) then) =
      __$CheckDeviceOutBodyModelCopyWithImpl<$Res>;
  @override
  $Res call({@JsonKey(name: 'userId') int userId});
}

class __$CheckDeviceOutBodyModelCopyWithImpl<$Res>
    extends _$CheckDeviceOutBodyModelCopyWithImpl<$Res>
    implements _$CheckDeviceOutBodyModelCopyWith<$Res> {
  __$CheckDeviceOutBodyModelCopyWithImpl(_CheckDeviceOutBodyModel _value,
      $Res Function(_CheckDeviceOutBodyModel) _then)
      : super(_value, (v) => _then(v as _CheckDeviceOutBodyModel));

  @override
  _CheckDeviceOutBodyModel get _value =>
      super._value as _CheckDeviceOutBodyModel;

  @override
  $Res call({
    Object userId = freezed,
  }) {
    return _then(_CheckDeviceOutBodyModel(
      userId: userId == freezed ? _value.userId : userId as int,
    ));
  }
}

@JsonSerializable()
class _$_CheckDeviceOutBodyModel
    with DiagnosticableTreeMixin
    implements _CheckDeviceOutBodyModel {
  _$_CheckDeviceOutBodyModel({@JsonKey(name: 'userId') this.userId});

  factory _$_CheckDeviceOutBodyModel.fromJson(Map<String, dynamic> json) =>
      _$_$_CheckDeviceOutBodyModelFromJson(json);

  @override
  @JsonKey(name: 'userId')
  final int userId;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CheckDeviceOutBodyModel(userId: $userId)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'CheckDeviceOutBodyModel'))
      ..add(DiagnosticsProperty('userId', userId));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CheckDeviceOutBodyModel &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(userId);

  @override
  _$CheckDeviceOutBodyModelCopyWith<_CheckDeviceOutBodyModel> get copyWith =>
      __$CheckDeviceOutBodyModelCopyWithImpl<_CheckDeviceOutBodyModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_CheckDeviceOutBodyModelToJson(this);
  }
}

abstract class _CheckDeviceOutBodyModel implements CheckDeviceOutBodyModel {
  factory _CheckDeviceOutBodyModel({@JsonKey(name: 'userId') int userId}) =
      _$_CheckDeviceOutBodyModel;

  factory _CheckDeviceOutBodyModel.fromJson(Map<String, dynamic> json) =
      _$_CheckDeviceOutBodyModel.fromJson;

  @override
  @JsonKey(name: 'userId')
  int get userId;
  @override
  _$CheckDeviceOutBodyModelCopyWith<_CheckDeviceOutBodyModel> get copyWith;
}
