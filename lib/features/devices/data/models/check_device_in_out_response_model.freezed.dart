// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'check_device_in_out_response_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
CheckDeviceInOutResponseModel _$CheckDeviceInOutResponseModelFromJson(
    Map<String, dynamic> json) {
  return _CheckDeviceInOutResponseModel.fromJson(json);
}

class _$CheckDeviceInOutResponseModelTearOff {
  const _$CheckDeviceInOutResponseModelTearOff();

  _CheckDeviceInOutResponseModel call(
      {@JsonKey(name: 'status') String status,
      @JsonKey(name: 'messages') String messages}) {
    return _CheckDeviceInOutResponseModel(
      status: status,
      messages: messages,
    );
  }
}

// ignore: unused_element
const $CheckDeviceInOutResponseModel = _$CheckDeviceInOutResponseModelTearOff();

mixin _$CheckDeviceInOutResponseModel {
  @JsonKey(name: 'status')
  String get status;
  @JsonKey(name: 'messages')
  String get messages;

  Map<String, dynamic> toJson();
  $CheckDeviceInOutResponseModelCopyWith<CheckDeviceInOutResponseModel>
      get copyWith;
}

abstract class $CheckDeviceInOutResponseModelCopyWith<$Res> {
  factory $CheckDeviceInOutResponseModelCopyWith(
          CheckDeviceInOutResponseModel value,
          $Res Function(CheckDeviceInOutResponseModel) then) =
      _$CheckDeviceInOutResponseModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'status') String status,
      @JsonKey(name: 'messages') String messages});
}

class _$CheckDeviceInOutResponseModelCopyWithImpl<$Res>
    implements $CheckDeviceInOutResponseModelCopyWith<$Res> {
  _$CheckDeviceInOutResponseModelCopyWithImpl(this._value, this._then);

  final CheckDeviceInOutResponseModel _value;
  // ignore: unused_field
  final $Res Function(CheckDeviceInOutResponseModel) _then;

  @override
  $Res call({
    Object status = freezed,
    Object messages = freezed,
  }) {
    return _then(_value.copyWith(
      status: status == freezed ? _value.status : status as String,
      messages: messages == freezed ? _value.messages : messages as String,
    ));
  }
}

abstract class _$CheckDeviceInOutResponseModelCopyWith<$Res>
    implements $CheckDeviceInOutResponseModelCopyWith<$Res> {
  factory _$CheckDeviceInOutResponseModelCopyWith(
          _CheckDeviceInOutResponseModel value,
          $Res Function(_CheckDeviceInOutResponseModel) then) =
      __$CheckDeviceInOutResponseModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'status') String status,
      @JsonKey(name: 'messages') String messages});
}

class __$CheckDeviceInOutResponseModelCopyWithImpl<$Res>
    extends _$CheckDeviceInOutResponseModelCopyWithImpl<$Res>
    implements _$CheckDeviceInOutResponseModelCopyWith<$Res> {
  __$CheckDeviceInOutResponseModelCopyWithImpl(
      _CheckDeviceInOutResponseModel _value,
      $Res Function(_CheckDeviceInOutResponseModel) _then)
      : super(_value, (v) => _then(v as _CheckDeviceInOutResponseModel));

  @override
  _CheckDeviceInOutResponseModel get _value =>
      super._value as _CheckDeviceInOutResponseModel;

  @override
  $Res call({
    Object status = freezed,
    Object messages = freezed,
  }) {
    return _then(_CheckDeviceInOutResponseModel(
      status: status == freezed ? _value.status : status as String,
      messages: messages == freezed ? _value.messages : messages as String,
    ));
  }
}

@JsonSerializable()
class _$_CheckDeviceInOutResponseModel
    with DiagnosticableTreeMixin
    implements _CheckDeviceInOutResponseModel {
  _$_CheckDeviceInOutResponseModel(
      {@JsonKey(name: 'status') this.status,
      @JsonKey(name: 'messages') this.messages});

  factory _$_CheckDeviceInOutResponseModel.fromJson(
          Map<String, dynamic> json) =>
      _$_$_CheckDeviceInOutResponseModelFromJson(json);

  @override
  @JsonKey(name: 'status')
  final String status;
  @override
  @JsonKey(name: 'messages')
  final String messages;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CheckDeviceInOutResponseModel(status: $status, messages: $messages)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'CheckDeviceInOutResponseModel'))
      ..add(DiagnosticsProperty('status', status))
      ..add(DiagnosticsProperty('messages', messages));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CheckDeviceInOutResponseModel &&
            (identical(other.status, status) ||
                const DeepCollectionEquality().equals(other.status, status)) &&
            (identical(other.messages, messages) ||
                const DeepCollectionEquality()
                    .equals(other.messages, messages)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(status) ^
      const DeepCollectionEquality().hash(messages);

  @override
  _$CheckDeviceInOutResponseModelCopyWith<_CheckDeviceInOutResponseModel>
      get copyWith => __$CheckDeviceInOutResponseModelCopyWithImpl<
          _CheckDeviceInOutResponseModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_CheckDeviceInOutResponseModelToJson(this);
  }
}

abstract class _CheckDeviceInOutResponseModel
    implements CheckDeviceInOutResponseModel {
  factory _CheckDeviceInOutResponseModel(
          {@JsonKey(name: 'status') String status,
          @JsonKey(name: 'messages') String messages}) =
      _$_CheckDeviceInOutResponseModel;

  factory _CheckDeviceInOutResponseModel.fromJson(Map<String, dynamic> json) =
      _$_CheckDeviceInOutResponseModel.fromJson;

  @override
  @JsonKey(name: 'status')
  String get status;
  @override
  @JsonKey(name: 'messages')
  String get messages;
  @override
  _$CheckDeviceInOutResponseModelCopyWith<_CheckDeviceInOutResponseModel>
      get copyWith;
}
