import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'rtd_location_model.freezed.dart';
part 'rtd_location_model.g.dart';

@freezed
abstract class RtdLocationModel with _$RtdLocationModel {
  factory RtdLocationModel({
    @JsonKey(name: 'id') int id,
    @JsonKey(name: 'name') String name,
  }) = _RtdLocationModel;

    factory RtdLocationModel.fromJson(Map<String, dynamic> json) => _$RtdLocationModelFromJson(json);
}
