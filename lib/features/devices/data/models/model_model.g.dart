// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ModelModel _$_$_ModelModelFromJson(Map<String, dynamic> json) {
  return _$_ModelModel(
    id: json['id'] as int,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$_$_ModelModelToJson(_$_ModelModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
