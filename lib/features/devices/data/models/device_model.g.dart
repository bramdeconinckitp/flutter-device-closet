// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_DeviceModel _$_$_DeviceModelFromJson(Map<String, dynamic> json) {
  return _$_DeviceModel(
    id: json['id'] as int,
    assetTag: json['asset_tag'] as String,
    image: json['image'] as String,
    userCanCheckout: json['user_can_checkout'] as bool,
    assignedTo: json['assigned_to'] == null
        ? null
        : UserModel.fromJson(json['assigned_to'] as Map<String, dynamic>),
    category: json['category'] == null
        ? null
        : CategoryModel.fromJson(json['category'] as Map<String, dynamic>),
    manufacturer: json['manufacturer'] == null
        ? null
        : ManufacturerModel.fromJson(
            json['manufacturer'] as Map<String, dynamic>),
    model: json['model'] == null
        ? null
        : ModelModel.fromJson(json['model'] as Map<String, dynamic>),
    rtdLocation: json['rtd_location'] == null
        ? null
        : RtdLocationModel.fromJson(
            json['rtd_location'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$_$_DeviceModelToJson(_$_DeviceModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'asset_tag': instance.assetTag,
      'image': instance.image,
      'user_can_checkout': instance.userCanCheckout,
      'assigned_to': instance.assignedTo,
      'category': instance.category,
      'manufacturer': instance.manufacturer,
      'model': instance.model,
      'rtd_location': instance.rtdLocation,
    };
