// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'manufacturer_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
ManufacturerModel _$ManufacturerModelFromJson(Map<String, dynamic> json) {
  return _ManufacturerModel.fromJson(json);
}

class _$ManufacturerModelTearOff {
  const _$ManufacturerModelTearOff();

  _ManufacturerModel call(
      {@JsonKey(name: 'id') int id, @JsonKey(name: 'name') String name}) {
    return _ManufacturerModel(
      id: id,
      name: name,
    );
  }
}

// ignore: unused_element
const $ManufacturerModel = _$ManufacturerModelTearOff();

mixin _$ManufacturerModel {
  @JsonKey(name: 'id')
  int get id;
  @JsonKey(name: 'name')
  String get name;

  Map<String, dynamic> toJson();
  $ManufacturerModelCopyWith<ManufacturerModel> get copyWith;
}

abstract class $ManufacturerModelCopyWith<$Res> {
  factory $ManufacturerModelCopyWith(
          ManufacturerModel value, $Res Function(ManufacturerModel) then) =
      _$ManufacturerModelCopyWithImpl<$Res>;
  $Res call({@JsonKey(name: 'id') int id, @JsonKey(name: 'name') String name});
}

class _$ManufacturerModelCopyWithImpl<$Res>
    implements $ManufacturerModelCopyWith<$Res> {
  _$ManufacturerModelCopyWithImpl(this._value, this._then);

  final ManufacturerModel _value;
  // ignore: unused_field
  final $Res Function(ManufacturerModel) _then;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as int,
      name: name == freezed ? _value.name : name as String,
    ));
  }
}

abstract class _$ManufacturerModelCopyWith<$Res>
    implements $ManufacturerModelCopyWith<$Res> {
  factory _$ManufacturerModelCopyWith(
          _ManufacturerModel value, $Res Function(_ManufacturerModel) then) =
      __$ManufacturerModelCopyWithImpl<$Res>;
  @override
  $Res call({@JsonKey(name: 'id') int id, @JsonKey(name: 'name') String name});
}

class __$ManufacturerModelCopyWithImpl<$Res>
    extends _$ManufacturerModelCopyWithImpl<$Res>
    implements _$ManufacturerModelCopyWith<$Res> {
  __$ManufacturerModelCopyWithImpl(
      _ManufacturerModel _value, $Res Function(_ManufacturerModel) _then)
      : super(_value, (v) => _then(v as _ManufacturerModel));

  @override
  _ManufacturerModel get _value => super._value as _ManufacturerModel;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
  }) {
    return _then(_ManufacturerModel(
      id: id == freezed ? _value.id : id as int,
      name: name == freezed ? _value.name : name as String,
    ));
  }
}

@JsonSerializable()
class _$_ManufacturerModel
    with DiagnosticableTreeMixin
    implements _ManufacturerModel {
  _$_ManufacturerModel(
      {@JsonKey(name: 'id') this.id, @JsonKey(name: 'name') this.name});

  factory _$_ManufacturerModel.fromJson(Map<String, dynamic> json) =>
      _$_$_ManufacturerModelFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'name')
  final String name;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ManufacturerModel(id: $id, name: $name)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ManufacturerModel'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('name', name));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ManufacturerModel &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name);

  @override
  _$ManufacturerModelCopyWith<_ManufacturerModel> get copyWith =>
      __$ManufacturerModelCopyWithImpl<_ManufacturerModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ManufacturerModelToJson(this);
  }
}

abstract class _ManufacturerModel implements ManufacturerModel {
  factory _ManufacturerModel(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'name') String name}) = _$_ManufacturerModel;

  factory _ManufacturerModel.fromJson(Map<String, dynamic> json) =
      _$_ManufacturerModel.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id;
  @override
  @JsonKey(name: 'name')
  String get name;
  @override
  _$ManufacturerModelCopyWith<_ManufacturerModel> get copyWith;
}
