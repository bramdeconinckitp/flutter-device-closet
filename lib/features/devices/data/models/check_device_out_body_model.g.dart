// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_device_out_body_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CheckDeviceOutBodyModel _$_$_CheckDeviceOutBodyModelFromJson(
    Map<String, dynamic> json) {
  return _$_CheckDeviceOutBodyModel(
    userId: json['userId'] as int,
  );
}

Map<String, dynamic> _$_$_CheckDeviceOutBodyModelToJson(
        _$_CheckDeviceOutBodyModel instance) =>
    <String, dynamic>{
      'userId': instance.userId,
    };
