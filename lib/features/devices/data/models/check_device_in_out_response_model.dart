import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'check_device_in_out_response_model.freezed.dart';
part 'check_device_in_out_response_model.g.dart';

@freezed
abstract class CheckDeviceInOutResponseModel with _$CheckDeviceInOutResponseModel {
  factory CheckDeviceInOutResponseModel({
    @JsonKey(name: 'status') String status,
    @JsonKey(name: 'messages') String messages,
  }) = _CheckDeviceInOutResponseModel;

  factory CheckDeviceInOutResponseModel.fromJson(Map<String, dynamic> json) => _$CheckDeviceInOutResponseModelFromJson(json);
}
