// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'device_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
DeviceModel _$DeviceModelFromJson(Map<String, dynamic> json) {
  return _DeviceModel.fromJson(json);
}

class _$DeviceModelTearOff {
  const _$DeviceModelTearOff();

  _DeviceModel call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'asset_tag') String assetTag,
      @JsonKey(name: 'image') String image,
      @JsonKey(name: 'user_can_checkout') bool userCanCheckout,
      @JsonKey(name: 'assigned_to') UserModel assignedTo,
      @JsonKey(name: 'category') CategoryModel category,
      @JsonKey(name: 'manufacturer') ManufacturerModel manufacturer,
      @JsonKey(name: 'model') ModelModel model,
      @JsonKey(name: 'rtd_location') RtdLocationModel rtdLocation}) {
    return _DeviceModel(
      id: id,
      assetTag: assetTag,
      image: image,
      userCanCheckout: userCanCheckout,
      assignedTo: assignedTo,
      category: category,
      manufacturer: manufacturer,
      model: model,
      rtdLocation: rtdLocation,
    );
  }
}

// ignore: unused_element
const $DeviceModel = _$DeviceModelTearOff();

mixin _$DeviceModel {
  @JsonKey(name: 'id')
  int get id;
  @JsonKey(name: 'asset_tag')
  String get assetTag;
  @JsonKey(name: 'image')
  String get image;
  @JsonKey(name: 'user_can_checkout')
  bool get userCanCheckout;
  @JsonKey(name: 'assigned_to')
  UserModel get assignedTo;
  @JsonKey(name: 'category')
  CategoryModel get category;
  @JsonKey(name: 'manufacturer')
  ManufacturerModel get manufacturer;
  @JsonKey(name: 'model')
  ModelModel get model;
  @JsonKey(name: 'rtd_location')
  RtdLocationModel get rtdLocation;

  Map<String, dynamic> toJson();
  $DeviceModelCopyWith<DeviceModel> get copyWith;
}

abstract class $DeviceModelCopyWith<$Res> {
  factory $DeviceModelCopyWith(
          DeviceModel value, $Res Function(DeviceModel) then) =
      _$DeviceModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'asset_tag') String assetTag,
      @JsonKey(name: 'image') String image,
      @JsonKey(name: 'user_can_checkout') bool userCanCheckout,
      @JsonKey(name: 'assigned_to') UserModel assignedTo,
      @JsonKey(name: 'category') CategoryModel category,
      @JsonKey(name: 'manufacturer') ManufacturerModel manufacturer,
      @JsonKey(name: 'model') ModelModel model,
      @JsonKey(name: 'rtd_location') RtdLocationModel rtdLocation});

  $UserModelCopyWith<$Res> get assignedTo;
  $CategoryModelCopyWith<$Res> get category;
  $ManufacturerModelCopyWith<$Res> get manufacturer;
  $ModelModelCopyWith<$Res> get model;
  $RtdLocationModelCopyWith<$Res> get rtdLocation;
}

class _$DeviceModelCopyWithImpl<$Res> implements $DeviceModelCopyWith<$Res> {
  _$DeviceModelCopyWithImpl(this._value, this._then);

  final DeviceModel _value;
  // ignore: unused_field
  final $Res Function(DeviceModel) _then;

  @override
  $Res call({
    Object id = freezed,
    Object assetTag = freezed,
    Object image = freezed,
    Object userCanCheckout = freezed,
    Object assignedTo = freezed,
    Object category = freezed,
    Object manufacturer = freezed,
    Object model = freezed,
    Object rtdLocation = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as int,
      assetTag: assetTag == freezed ? _value.assetTag : assetTag as String,
      image: image == freezed ? _value.image : image as String,
      userCanCheckout: userCanCheckout == freezed
          ? _value.userCanCheckout
          : userCanCheckout as bool,
      assignedTo:
          assignedTo == freezed ? _value.assignedTo : assignedTo as UserModel,
      category:
          category == freezed ? _value.category : category as CategoryModel,
      manufacturer: manufacturer == freezed
          ? _value.manufacturer
          : manufacturer as ManufacturerModel,
      model: model == freezed ? _value.model : model as ModelModel,
      rtdLocation: rtdLocation == freezed
          ? _value.rtdLocation
          : rtdLocation as RtdLocationModel,
    ));
  }

  @override
  $UserModelCopyWith<$Res> get assignedTo {
    if (_value.assignedTo == null) {
      return null;
    }
    return $UserModelCopyWith<$Res>(_value.assignedTo, (value) {
      return _then(_value.copyWith(assignedTo: value));
    });
  }

  @override
  $CategoryModelCopyWith<$Res> get category {
    if (_value.category == null) {
      return null;
    }
    return $CategoryModelCopyWith<$Res>(_value.category, (value) {
      return _then(_value.copyWith(category: value));
    });
  }

  @override
  $ManufacturerModelCopyWith<$Res> get manufacturer {
    if (_value.manufacturer == null) {
      return null;
    }
    return $ManufacturerModelCopyWith<$Res>(_value.manufacturer, (value) {
      return _then(_value.copyWith(manufacturer: value));
    });
  }

  @override
  $ModelModelCopyWith<$Res> get model {
    if (_value.model == null) {
      return null;
    }
    return $ModelModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }

  @override
  $RtdLocationModelCopyWith<$Res> get rtdLocation {
    if (_value.rtdLocation == null) {
      return null;
    }
    return $RtdLocationModelCopyWith<$Res>(_value.rtdLocation, (value) {
      return _then(_value.copyWith(rtdLocation: value));
    });
  }
}

abstract class _$DeviceModelCopyWith<$Res>
    implements $DeviceModelCopyWith<$Res> {
  factory _$DeviceModelCopyWith(
          _DeviceModel value, $Res Function(_DeviceModel) then) =
      __$DeviceModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'asset_tag') String assetTag,
      @JsonKey(name: 'image') String image,
      @JsonKey(name: 'user_can_checkout') bool userCanCheckout,
      @JsonKey(name: 'assigned_to') UserModel assignedTo,
      @JsonKey(name: 'category') CategoryModel category,
      @JsonKey(name: 'manufacturer') ManufacturerModel manufacturer,
      @JsonKey(name: 'model') ModelModel model,
      @JsonKey(name: 'rtd_location') RtdLocationModel rtdLocation});

  @override
  $UserModelCopyWith<$Res> get assignedTo;
  @override
  $CategoryModelCopyWith<$Res> get category;
  @override
  $ManufacturerModelCopyWith<$Res> get manufacturer;
  @override
  $ModelModelCopyWith<$Res> get model;
  @override
  $RtdLocationModelCopyWith<$Res> get rtdLocation;
}

class __$DeviceModelCopyWithImpl<$Res> extends _$DeviceModelCopyWithImpl<$Res>
    implements _$DeviceModelCopyWith<$Res> {
  __$DeviceModelCopyWithImpl(
      _DeviceModel _value, $Res Function(_DeviceModel) _then)
      : super(_value, (v) => _then(v as _DeviceModel));

  @override
  _DeviceModel get _value => super._value as _DeviceModel;

  @override
  $Res call({
    Object id = freezed,
    Object assetTag = freezed,
    Object image = freezed,
    Object userCanCheckout = freezed,
    Object assignedTo = freezed,
    Object category = freezed,
    Object manufacturer = freezed,
    Object model = freezed,
    Object rtdLocation = freezed,
  }) {
    return _then(_DeviceModel(
      id: id == freezed ? _value.id : id as int,
      assetTag: assetTag == freezed ? _value.assetTag : assetTag as String,
      image: image == freezed ? _value.image : image as String,
      userCanCheckout: userCanCheckout == freezed
          ? _value.userCanCheckout
          : userCanCheckout as bool,
      assignedTo:
          assignedTo == freezed ? _value.assignedTo : assignedTo as UserModel,
      category:
          category == freezed ? _value.category : category as CategoryModel,
      manufacturer: manufacturer == freezed
          ? _value.manufacturer
          : manufacturer as ManufacturerModel,
      model: model == freezed ? _value.model : model as ModelModel,
      rtdLocation: rtdLocation == freezed
          ? _value.rtdLocation
          : rtdLocation as RtdLocationModel,
    ));
  }
}

@JsonSerializable()
class _$_DeviceModel with DiagnosticableTreeMixin implements _DeviceModel {
  _$_DeviceModel(
      {@JsonKey(name: 'id') this.id,
      @JsonKey(name: 'asset_tag') this.assetTag,
      @JsonKey(name: 'image') this.image,
      @JsonKey(name: 'user_can_checkout') this.userCanCheckout,
      @JsonKey(name: 'assigned_to') this.assignedTo,
      @JsonKey(name: 'category') this.category,
      @JsonKey(name: 'manufacturer') this.manufacturer,
      @JsonKey(name: 'model') this.model,
      @JsonKey(name: 'rtd_location') this.rtdLocation});

  factory _$_DeviceModel.fromJson(Map<String, dynamic> json) =>
      _$_$_DeviceModelFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'asset_tag')
  final String assetTag;
  @override
  @JsonKey(name: 'image')
  final String image;
  @override
  @JsonKey(name: 'user_can_checkout')
  final bool userCanCheckout;
  @override
  @JsonKey(name: 'assigned_to')
  final UserModel assignedTo;
  @override
  @JsonKey(name: 'category')
  final CategoryModel category;
  @override
  @JsonKey(name: 'manufacturer')
  final ManufacturerModel manufacturer;
  @override
  @JsonKey(name: 'model')
  final ModelModel model;
  @override
  @JsonKey(name: 'rtd_location')
  final RtdLocationModel rtdLocation;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'DeviceModel(id: $id, assetTag: $assetTag, image: $image, userCanCheckout: $userCanCheckout, assignedTo: $assignedTo, category: $category, manufacturer: $manufacturer, model: $model, rtdLocation: $rtdLocation)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'DeviceModel'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('assetTag', assetTag))
      ..add(DiagnosticsProperty('image', image))
      ..add(DiagnosticsProperty('userCanCheckout', userCanCheckout))
      ..add(DiagnosticsProperty('assignedTo', assignedTo))
      ..add(DiagnosticsProperty('category', category))
      ..add(DiagnosticsProperty('manufacturer', manufacturer))
      ..add(DiagnosticsProperty('model', model))
      ..add(DiagnosticsProperty('rtdLocation', rtdLocation));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _DeviceModel &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.assetTag, assetTag) ||
                const DeepCollectionEquality()
                    .equals(other.assetTag, assetTag)) &&
            (identical(other.image, image) ||
                const DeepCollectionEquality().equals(other.image, image)) &&
            (identical(other.userCanCheckout, userCanCheckout) ||
                const DeepCollectionEquality()
                    .equals(other.userCanCheckout, userCanCheckout)) &&
            (identical(other.assignedTo, assignedTo) ||
                const DeepCollectionEquality()
                    .equals(other.assignedTo, assignedTo)) &&
            (identical(other.category, category) ||
                const DeepCollectionEquality()
                    .equals(other.category, category)) &&
            (identical(other.manufacturer, manufacturer) ||
                const DeepCollectionEquality()
                    .equals(other.manufacturer, manufacturer)) &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)) &&
            (identical(other.rtdLocation, rtdLocation) ||
                const DeepCollectionEquality()
                    .equals(other.rtdLocation, rtdLocation)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(assetTag) ^
      const DeepCollectionEquality().hash(image) ^
      const DeepCollectionEquality().hash(userCanCheckout) ^
      const DeepCollectionEquality().hash(assignedTo) ^
      const DeepCollectionEquality().hash(category) ^
      const DeepCollectionEquality().hash(manufacturer) ^
      const DeepCollectionEquality().hash(model) ^
      const DeepCollectionEquality().hash(rtdLocation);

  @override
  _$DeviceModelCopyWith<_DeviceModel> get copyWith =>
      __$DeviceModelCopyWithImpl<_DeviceModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_DeviceModelToJson(this);
  }
}

abstract class _DeviceModel implements DeviceModel {
  factory _DeviceModel(
          {@JsonKey(name: 'id') int id,
          @JsonKey(name: 'asset_tag') String assetTag,
          @JsonKey(name: 'image') String image,
          @JsonKey(name: 'user_can_checkout') bool userCanCheckout,
          @JsonKey(name: 'assigned_to') UserModel assignedTo,
          @JsonKey(name: 'category') CategoryModel category,
          @JsonKey(name: 'manufacturer') ManufacturerModel manufacturer,
          @JsonKey(name: 'model') ModelModel model,
          @JsonKey(name: 'rtd_location') RtdLocationModel rtdLocation}) =
      _$_DeviceModel;

  factory _DeviceModel.fromJson(Map<String, dynamic> json) =
      _$_DeviceModel.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id;
  @override
  @JsonKey(name: 'asset_tag')
  String get assetTag;
  @override
  @JsonKey(name: 'image')
  String get image;
  @override
  @JsonKey(name: 'user_can_checkout')
  bool get userCanCheckout;
  @override
  @JsonKey(name: 'assigned_to')
  UserModel get assignedTo;
  @override
  @JsonKey(name: 'category')
  CategoryModel get category;
  @override
  @JsonKey(name: 'manufacturer')
  ManufacturerModel get manufacturer;
  @override
  @JsonKey(name: 'model')
  ModelModel get model;
  @override
  @JsonKey(name: 'rtd_location')
  RtdLocationModel get rtdLocation;
  @override
  _$DeviceModelCopyWith<_DeviceModel> get copyWith;
}
