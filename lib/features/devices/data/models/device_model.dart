import 'package:flutter_device_closet/features/devices/data/models/category_model.dart';
import 'package:flutter_device_closet/features/devices/data/models/manufacturer_model.dart';
import 'package:flutter_device_closet/features/devices/data/models/model_model.dart';
import 'package:flutter_device_closet/features/devices/data/models/rtd_location_model.dart';
import 'package:flutter_device_closet/features/users/data/models/user_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'device_model.freezed.dart';
part 'device_model.g.dart';

@freezed
abstract class DeviceModel with _$DeviceModel {
  factory DeviceModel({
    @JsonKey(name: 'id') int id,
    @JsonKey(name: 'asset_tag') String assetTag,
    @JsonKey(name: 'image') String image,
    @JsonKey(name: 'user_can_checkout') bool userCanCheckout,
    @JsonKey(name: 'assigned_to') UserModel assignedTo,
    @JsonKey(name: 'category') CategoryModel category,
    @JsonKey(name: 'manufacturer') ManufacturerModel manufacturer,
    @JsonKey(name: 'model') ModelModel model,
    @JsonKey(name: 'rtd_location') RtdLocationModel rtdLocation,
  }) = _DeviceModel;

  factory DeviceModel.fromJson(Map<String, dynamic> json) => _$DeviceModelFromJson(json);
}
