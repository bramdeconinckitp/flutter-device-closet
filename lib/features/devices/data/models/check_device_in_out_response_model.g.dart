// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_device_in_out_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CheckDeviceInOutResponseModel _$_$_CheckDeviceInOutResponseModelFromJson(
    Map<String, dynamic> json) {
  return _$_CheckDeviceInOutResponseModel(
    status: json['status'] as String,
    messages: json['messages'] as String,
  );
}

Map<String, dynamic> _$_$_CheckDeviceInOutResponseModelToJson(
        _$_CheckDeviceInOutResponseModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'messages': instance.messages,
    };
