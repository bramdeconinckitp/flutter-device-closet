// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'manufacturer_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ManufacturerModel _$_$_ManufacturerModelFromJson(Map<String, dynamic> json) {
  return _$_ManufacturerModel(
    id: json['id'] as int,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$_$_ManufacturerModelToJson(
        _$_ManufacturerModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
