import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'manufacturer_model.freezed.dart';
part 'manufacturer_model.g.dart';

@freezed
abstract class ManufacturerModel with _$ManufacturerModel {
  factory ManufacturerModel({
    @JsonKey(name: 'id') int id,
    @JsonKey(name: 'name') String name,
  }) = _ManufacturerModel;

    factory ManufacturerModel.fromJson(Map<String, dynamic> json) => _$ManufacturerModelFromJson(json);
}
