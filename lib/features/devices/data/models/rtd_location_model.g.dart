// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rtd_location_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_RtdLocationModel _$_$_RtdLocationModelFromJson(Map<String, dynamic> json) {
  return _$_RtdLocationModel(
    id: json['id'] as int,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$_$_RtdLocationModelToJson(
        _$_RtdLocationModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
