import 'package:flutter/foundation.dart';
import 'package:flutter_device_closet/features/devices/business/entities/format.dart';
import 'package:flutter_device_closet/features/users/business/entities/user.dart';

class Device {
  final int id;
  final String tag;
  final String brand;
  final String model;
  final String imageUrl;
  final String location;
  final bool isAvailable;
  final Format format;
  final User assignee;

  Device({
    @required this.id,
    @required this.tag,
    @required this.brand,
    @required this.model,
    @required this.imageUrl,
    @required this.location,
    @required this.isAvailable,
    @required this.format,
    @required this.assignee,
  });

  String get fullName => '$brand $model';
}
