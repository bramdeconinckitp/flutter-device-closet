enum Format {
  smartphone,
  tablet,
  other,
}

extension FormatExtensions on Format {
  String get stringValue {
    switch (this) {
      case Format.smartphone:
        return 'Smartphone';
      case Format.tablet:
        return 'Tablet';
      default:
        return '';
    }
  }
}

Format parseFormat(String format) {
  switch (format.toLowerCase()) {
    case 'smartphones':
      return Format.smartphone;
    case 'tablets':
      return Format.tablet;
    default:
      return Format.other;
  }
}
