import 'package:flutter_device_closet/features/devices/business/entities/device.dart';
import 'package:flutter_device_closet/features/users/business/entities/user.dart';
import 'package:rxdart/rxdart.dart';

abstract class DeviceRepository {
  Observable<List<Device>> getAllDevices();
  Observable<Device> getDeviceById(String deviceId);
  Future<bool> checkDeviceOut(Device device, User user);
  Future<bool> checkDeviceIn(Device device);
}
