import 'package:flutter_device_closet/features/devices/business/entities/device.dart';
import 'package:flutter_device_closet/features/devices/business/repositories/device_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@lazySingleton
class GetAllDevicesUseCase{
  final DeviceRepository _deviceRepository;

  GetAllDevicesUseCase(this._deviceRepository);

  Observable<List<Device>> call() {
    return _deviceRepository.getAllDevices();
  }
}
