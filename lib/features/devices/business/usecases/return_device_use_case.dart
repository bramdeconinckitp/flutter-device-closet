import 'package:flutter_device_closet/features/devices/business/entities/device.dart';
import 'package:flutter_device_closet/features/devices/business/repositories/device_repository.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class ReturnDeviceUseCase {
  final DeviceRepository _deviceRepository;

  ReturnDeviceUseCase(this._deviceRepository);

  Future<bool> call(Device device) {
    return _deviceRepository.checkDeviceIn(device);
  }
}
