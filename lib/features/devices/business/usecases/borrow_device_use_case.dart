import 'package:flutter_device_closet/core/services/navigation/router/navigation_router.dart';
import 'package:flutter_device_closet/core/services/navigation/service/navigation_service.dart';
import 'package:flutter_device_closet/features/devices/business/entities/device.dart';
import 'package:flutter_device_closet/features/devices/business/repositories/device_repository.dart';
import 'package:flutter_device_closet/features/users/business/entities/user.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class BorrowDeviceUseCase {
  final DeviceRepository _deviceRepository;
  final NavigationService _navigationService;

  BorrowDeviceUseCase(
    this._deviceRepository,
    this._navigationService,
  );

  Future<bool> call(Device device) async {
    final dynamic user = await _navigationService.navigateTo(NavigationRouter.users);

    if (user == null || user is! User) {
      return null;
    }

    return _deviceRepository.checkDeviceOut(device, user as User);
  }
}
