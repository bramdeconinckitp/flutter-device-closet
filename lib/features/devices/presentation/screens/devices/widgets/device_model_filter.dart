import 'package:flutter/material.dart';
import 'package:flutter_device_closet/core/constants/app_colors.dart';
import 'package:flutter_device_closet/features/devices/presentation/blocs/device_bloc.dart';
import 'package:provider/provider.dart';

class DeviceModelFilter extends StatefulWidget {
  @override
  _DeviceModelFilterState createState() => _DeviceModelFilterState();
}

class _DeviceModelFilterState extends State<DeviceModelFilter> {
  TextEditingController _modelFilterController;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final deviceBloc = Provider.of<DeviceBloc>(context);
    final modelFilterValue = deviceBloc.currentModelFilterValue;
    _modelFilterController = TextEditingController(text: modelFilterValue);
  }

  @override
  void dispose() {
    _modelFilterController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deviceBloc = Provider.of<DeviceBloc>(context);

    return TextField(
      keyboardType: TextInputType.text,
      onChanged: deviceBloc.changeModelFilter,
      maxLines: 1,
      controller: _modelFilterController,
      decoration: const InputDecoration(
        hintText: 'Device model name..',
        hintMaxLines: 1,
        contentPadding: EdgeInsets.symmetric(
          horizontal: 8,
          vertical: 15,
        ),
        enabledBorder: UnderlineInputBorder(
          borderRadius: BorderRadius.all(Radius.zero),
          borderSide: BorderSide(
            width: 1,
            color: AppColors.searchFieldGrey,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderRadius: BorderRadius.all(Radius.zero),
          borderSide: BorderSide(
            width: 1,
            color: AppColors.searchFieldGrey,
          ),
        ),
      ),
    );
  }
}
