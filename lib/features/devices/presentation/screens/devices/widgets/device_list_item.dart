import 'package:flutter/material.dart';
import 'package:flutter_device_closet/core/constants/app_colors.dart';
import 'package:flutter_device_closet/core/constants/app_dimensions.dart';
import 'package:flutter_device_closet/core/extensions/build_context_extensions.dart';
import 'package:flutter_device_closet/core/extensions/string_extensions.dart';
import 'package:flutter_device_closet/features/devices/business/entities/device.dart';
import 'package:flutter_device_closet/features/devices/business/entities/format.dart';
import 'package:flutter_device_closet/features/devices/presentation/blocs/device_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';

class DevicesListItem extends StatelessWidget {
  final Device device;

  const DevicesListItem({@required this.device});

  void _onDeviceCardTapped(BuildContext context) {
    context.hideKeyboard();

    final deviceBloc = Provider.of<DeviceBloc>(context, listen: false);
    deviceBloc.onDeviceCardTapped(device);
  }

  void _onBorrowButtonTapped(BuildContext context) {
    final deviceBloc = Provider.of<DeviceBloc>(context, listen: false);
    deviceBloc.onBorrowButtonTapped(device);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      margin: const EdgeInsets.symmetric(
        horizontal: AppDimensions.paddingMedium,
        vertical: AppDimensions.paddingSmall,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      child: InkWell(
        borderRadius: BorderRadius.circular(8),
        onTap: () => _onDeviceCardTapped(context),
        child: _deviceDetails(context),
      ),
    );
  }

  Widget _deviceDetails(BuildContext context) {
    return Row(
      children: <Widget>[
        _deviceImage(context),
        const SizedBox(height: AppDimensions.paddingMedium),
        Expanded(
          child: _deviceInfo(),
        ),
        const SizedBox(height: AppDimensions.paddingMedium),
        _borrowButton(context),
      ],
    );
  }

  Widget _deviceImage(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      padding: const EdgeInsets.symmetric(
        horizontal: 8,
        vertical: 16,
      ),
      child: device.imageUrl.isNullOrEmpty
          ? Icon(
              device.format == Format.smartphone
                  ? Icons.smartphone
                  : device.format == Format.tablet ? FontAwesomeIcons.tablet : Icons.devices_other,
              size: 46,
              color: Colors.black87,
            )
          : Hero(
              tag: device.id,
              child: FadeInImage.memoryNetwork(
                image: device.imageUrl,
                placeholder: kTransparentImage,
              ),
            ),
    );
  }

  Widget _deviceInfo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _deviceBrandAndModel(),
        const SizedBox(height: AppDimensions.paddingMedium),
        _deviceTag(),
        const SizedBox(height: AppDimensions.paddingMedium),
        _deviceLocation(),
      ],
    );
  }

  Widget _deviceBrandAndModel() {
    return Text(
      device.fullName,
      maxLines: 1,
      style: const TextStyle(
        color: AppColors.primaryTextColor,
        fontSize: 16,
      ),
    );
  }

  Widget _deviceTag() {
    return Text(
      device.tag,
      maxLines: 1,
      style: const TextStyle(
        color: AppColors.secondaryTextColor,
        fontSize: 14,
      ),
    );
  }

  Widget _deviceLocation() {
    return Text(
      device.assignee == null ? device.location : device.assignee.name,
      maxLines: 1,
      style: const TextStyle(
        color: AppColors.secondaryTextColor,
        fontSize: 14,
      ),
    );
  }

  Widget _borrowButton(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      child: MaterialButton(
        onPressed: () => _onBorrowButtonTapped(context),
        textColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        color: device.isAvailable ? Colors.green : Colors.red,
        child: Text(
          device.isAvailable ? 'Borrow' : 'Return',
          style: const TextStyle(fontSize: 14),
        ),
      ),
    );
  }
}
