import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_device_closet/core/constants/app_colors.dart';
import 'package:flutter_device_closet/core/constants/app_dimensions.dart';
import 'package:flutter_device_closet/core/extensions/build_context_extensions.dart';
import 'package:flutter_device_closet/core/widgets/general/basic_app_bar.dart';
import 'package:flutter_device_closet/core/widgets/general/basic_empty_state.dart';
import 'package:flutter_device_closet/core/widgets/general/loading_indicator.dart';
import 'package:flutter_device_closet/features/devices/business/entities/device.dart';
import 'package:flutter_device_closet/features/devices/presentation/blocs/device_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import 'widgets/device_list_item.dart';
import 'widgets/device_model_filter.dart';

class DevicesScreen extends StatelessWidget {
  final List<Widget> _filterIcons = List<Widget>.unmodifiable(<Icon>[
    const Icon(Icons.smartphone),
    const Icon(FontAwesomeIcons.tablet),
    const Icon(Icons.devices_other),
  ]);

  void _onDeviceScreenTapped(BuildContext context) {
    context.hideKeyboard();
  }

  @override
  Widget build(BuildContext context) {
    final deviceBloc = Provider.of<DeviceBloc>(context);

    return GestureDetector(
      onTap: () => _onDeviceScreenTapped(context),
      child: Scaffold(
        appBar: const BasicAppBar('Devices'),
        body: _buildBody(deviceBloc),
      ),
    );
  }

  Widget _buildBody(DeviceBloc deviceBloc) {
    return StreamBuilder(
      stream: deviceBloc.filteredDevices,
      builder: (context, AsyncSnapshot<List<Device>> snapshot) {
        if (!snapshot.hasData) {
          return const LoadingIndicator();
        }

        return Column(
          children: <Widget>[
            _buildFilters(deviceBloc),
            const SizedBox(height: AppDimensions.paddingMedium),
            Expanded(
              child: _buildDeviceList(snapshot.data),
            ),
          ],
        );
      },
    );
  }

  Widget _buildFilters(DeviceBloc deviceBloc) {
    return Row(
      children: <Widget>[
        Expanded(
          child: DeviceModelFilter(),
        ),
        _buildPlatformAndFormatFilters(deviceBloc),
      ],
    );
  }

  Widget _buildPlatformAndFormatFilters(DeviceBloc deviceBloc) {
    return StreamBuilder(
      initialData: List<bool>.generate(_filterIcons.length, (_) => false),
      stream: deviceBloc.selectedFilters,
      builder: (context, AsyncSnapshot<List<bool>> snapshot) {
        return ToggleButtons(
          isSelected: snapshot.data,
          onPressed: deviceBloc.onFilterToggle,
          color: AppColors.primaryTextColor,
          disabledBorderColor: AppColors.searchFieldGrey,
          selectedBorderColor: AppColors.searchFieldGrey,
          borderColor: AppColors.searchFieldGrey,
          children: _filterIcons,
        );
      },
    );
  }

  Widget _buildDeviceList(List<Device> devices) {
    if (devices.isEmpty) {
      return const BasicEmptyState(text: 'No devices availble');
    }

    return ListView.builder(
      padding: const EdgeInsets.only(bottom: AppDimensions.paddingMedium),
      itemCount: devices.length,
      itemBuilder: (context, int index) {
        final device = devices[index];
        return DevicesListItem(device: device);
      },
    );
  }
}
