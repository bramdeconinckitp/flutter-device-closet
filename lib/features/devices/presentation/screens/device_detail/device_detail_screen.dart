import 'package:flutter/material.dart';
import 'package:flutter_device_closet/core/constants/app_dimensions.dart';
import 'package:flutter_device_closet/core/extensions/string_extensions.dart';
import 'package:flutter_device_closet/core/widgets/general/basic_app_bar.dart';
import 'package:flutter_device_closet/features/devices/business/entities/device.dart';
import 'package:flutter_device_closet/features/devices/business/entities/format.dart';

import 'widgets/device_text_field.dart';

class DeviceDetailScreen extends StatelessWidget {
  final int deviceId;

  const DeviceDetailScreen({@required this.deviceId});

  @override
  Widget build(BuildContext context) {
    final device = ModalRoute.of(context).settings.arguments as Device;

    return Scaffold(
      appBar: BasicAppBar(device.fullName),
      body: _buildBody(device, context),
      resizeToAvoidBottomInset: false,
    );
  }

  Widget _buildBody(Device device, BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 12),
      child: Column(
        children: <Widget>[
          _buildDeviceInfo(device, context),
        ],
      ),
    );
  }

  Widget _buildDeviceInfo(Device device, BuildContext context) {
    return Expanded(
      child: ListView(
        padding: const EdgeInsets.fromLTRB(16, 16, 16, 12),
        children: <Widget>[
          _buildDeviceImage(device, context),
          const SizedBox(height: AppDimensions.paddingLarge),
          _buildDeviceForm(device, context),
        ],
      ),
    );
  }

  Widget _buildDeviceImage(Device device, BuildContext context) {
    final height = MediaQuery.of(context).size.width * 2 / 3;

    return device.imageUrl.isNullOrEmpty
        ? Container()
        : Hero(
            tag: device.id,
            child: Image.network(
              device.imageUrl,
              height: height,
              fit: BoxFit.fitHeight,
            ),
          );
  }

  Widget _buildDeviceForm(Device device, BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        DeviceTextField(
          label: 'ID',
          value: device.id.toString(),
        ),
        DeviceTextField(
          label: 'Asset tag',
          value: device.tag,
        ),
        DeviceTextField(
          label: 'Manufacturer',
          value: device.brand,
        ),
        DeviceTextField(
          label: 'Model',
          value: device.model,
        ),
        DeviceTextField(
          label: 'Format',
          value: device.format.stringValue,
        ),
        DeviceTextField(
          label: 'Default location',
          value: device.location,
        ),
        if (device.assignee != null)
          DeviceTextField(
            label: 'Current assignee',
            value: device.assignee?.name,
          ),
      ],
    );
  }
}
