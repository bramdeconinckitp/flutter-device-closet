import 'package:flutter/material.dart';

class DeviceTextField extends StatelessWidget {
  final String label;
  final String value;

  const DeviceTextField({
    @required this.label,
    @required this.value,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      readOnly: true,
      initialValue: value ?? '',
      decoration: InputDecoration(
        labelText: label,
      ),
    );
  }
}
