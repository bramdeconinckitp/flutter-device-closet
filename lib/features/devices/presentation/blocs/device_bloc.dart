import 'dart:async';

import 'package:flutter_device_closet/core/services/dialogs/dialog_service.dart';
import 'package:flutter_device_closet/core/services/navigation/router/navigation_router.dart';
import 'package:flutter_device_closet/core/services/navigation/service/navigation_service.dart';
import 'package:flutter_device_closet/features/devices/business/entities/device.dart';
import 'package:flutter_device_closet/features/devices/business/entities/format.dart';
import 'package:flutter_device_closet/core/extensions/string_extensions.dart';
import 'package:flutter_device_closet/features/devices/business/usecases/borrow_device_use_case.dart';
import 'package:flutter_device_closet/features/devices/business/usecases/get_all_devices_use_case.dart';
import 'package:flutter_device_closet/features/devices/business/usecases/return_device_use_case.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@injectable
class DeviceBloc {
  final GetAllDevicesUseCase _getAllDevicesUseCase;
  final BorrowDeviceUseCase _borrowDeviceUseCase;
  final ReturnDeviceUseCase _returnDeviceUseCase;
  final NavigationService _navigationService;
  final DialogService _dialogService;

  final _filteredDevices = BehaviorSubject<List<Device>>();
  StreamSubscription<List<Device>> _filteredDevicesSubscription;
  Observable<List<Device>> get filteredDevices => _filteredDevices.stream;
  Function(List<Device>) get changeFilteredDevices => _filteredDevices.sink.add;

  final _modelFilter = BehaviorSubject<String>.seeded(null);
  String get currentModelFilterValue => _modelFilter.value;
  Observable<String> get modelFilter => _modelFilter.stream;
  Function(String) get changeModelFilter => _modelFilter.sink.add;

  final _formatFilter = BehaviorSubject<Format>.seeded(null);
  Observable<Format> get formatFilter => _formatFilter.stream;
  Function(Format) get changeFormatFilter => _formatFilter.sink.add;

  DeviceBloc(
    this._getAllDevicesUseCase,
    this._borrowDeviceUseCase,
    this._returnDeviceUseCase,
    this._navigationService,
    this._dialogService,
  ) {
    _getFilteredDevices();
  }

  void _getFilteredDevices() {
    _filteredDevicesSubscription = Observable.combineLatest3(_getAllDevicesUseCase(), modelFilter, formatFilter,
        (List<Device> devices, String name, Format format) {
      final filteredDevices = [...devices];
      name = name?.toLowerCase();

      if (!name.isNullOrEmpty) {
        filteredDevices.removeWhere((device) =>
            !device.fullName.toLowerCase().contains(name) &&
            !device.tag.toLowerCase().contains(name) &&
            (device.assignee == null || !device.assignee.name.toLowerCase().contains(name)));
      }

      if (format != null) {
        filteredDevices.removeWhere((device) => device.format != format);
      }

      return filteredDevices;
    }).listen(changeFilteredDevices);
  }

  Observable<List<bool>> get selectedFilters {
    return formatFilter.map((format) {
      return List<bool>.unmodifiable(<bool>[
        format == Format.smartphone,
        format == Format.tablet,
        format == Format.other,
      ]);
    });
  }

  void onFilterToggle(int index) {
    final selectedFormat = _formatFilter.value;

    switch (index) {
      case 0:
        changeFormatFilter(selectedFormat != Format.smartphone ? Format.smartphone : null);
        break;
      case 1:
        changeFormatFilter(selectedFormat != Format.tablet ? Format.tablet : null);
        break;
      case 2:
        changeFormatFilter(selectedFormat != Format.other ? Format.other : null);
        break;
    }
  }

  void onDeviceCardTapped(Device device) {
    final route = NavigationRouter.getDeviceDetailRoute(device.id);
    _navigationService.navigateTo(route, device);
  }

  Future<void> onBorrowButtonTapped(Device device) async {
    device.isAvailable ? await _borrowDevice(device) : await _returnDevice(device);
  }

  Future<void> _borrowDevice(Device device) async {
    final currentDevices = _filteredDevices.value;
    changeFilteredDevices(null);

    final success = await _borrowDeviceUseCase(device);

    if (success == null) {
      changeFilteredDevices(currentDevices);
      return;
    }

    _filteredDevicesSubscription?.cancel();
    _getFilteredDevices();

    success ? _presentDeviceBorrowSuccessDialog(device) : _presentDeviceBorrowFailureDialog(device);
  }

  void _presentDeviceBorrowSuccessDialog(Device device) {
    _dialogService.presentDialog(
      title: 'Device borrowed',
      description: 'The ${device.fullName} with asset tag ${device.tag} was borrowed successfully!',
      primaryButtonTitle: 'Ok',
    );
  }

  void _presentDeviceBorrowFailureDialog(Device device) {
    _dialogService.presentDialog(
      title: 'Error occurred',
      description:
          'An error occurred while borrowing the (${device.fullName}) with asset tag ${device.tag}. Please try again.',
      primaryButtonTitle: 'Ok',
    );
  }

  Future<void> _returnDevice(Device device) async {
    final currentDevices = _filteredDevices.value;
    changeFilteredDevices(null);

    final success = await _returnDeviceUseCase(device);

    if (success == null) {
      changeFilteredDevices(currentDevices);
      return;
    }

    _filteredDevicesSubscription?.cancel();
    _getFilteredDevices();

    success ? _presentDeviceReturnSuccessDialog(device) : _presentDeviceReturnFailureDialog(device);
  }

  void _presentDeviceReturnSuccessDialog(Device device) {
    _dialogService.presentDialog(
      title: 'Device returned',
      description: 'The ${device.fullName} with asset tag ${device.tag} was returned successfully!',
      primaryButtonTitle: 'Ok',
    );
  }

  void _presentDeviceReturnFailureDialog(Device device) {
    _dialogService.presentDialog(
      title: 'Error occurred',
      description:
          'An error occurred while returning the (${device.fullName}) with asset tag ${device.tag}. Please try again.',
      primaryButtonTitle: 'Ok',
    );
  }

  void dispose() {
    _filteredDevices?.close();
    _filteredDevicesSubscription?.cancel();
    _modelFilter?.close();
    _formatFilter?.close();
  }
}
