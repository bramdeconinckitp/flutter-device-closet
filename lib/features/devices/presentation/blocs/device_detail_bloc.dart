import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@injectable
class DeviceDetailBloc {
  // final BorrowDeviceUseCase _borrowDeviceUseCase;
  // final ReturnBorrowedDeviceUseCase _returnBorrowedDeviceUseCase;

  final BehaviorSubject<bool> _isLoading = BehaviorSubject.seeded(false);
  Observable<bool> get isLoading => _isLoading.stream;
  Function(bool) get changeIsLoading => _isLoading.sink.add;

  // DeviceDetailBloc(
  //   this._borrowDeviceUseCase,
  //   this._returnBorrowedDeviceUseCase,
  // );

  // Observable<BorrowingStatus> getDeviceBorrowingStatus(String deviceId) {
  //   return _getDeviceBorrowingStatusUseCase(deviceId);
  // }

  // Future<bool> onBorrowDevicePressed(String deviceId) async {
  //   changeIsLoading(true);
  //   final result = await _borrowDeviceUseCase(deviceId);
  //   changeIsLoading(false);

  //   return result;
  // }

  // Future<bool> onReturnBorrowedDevicePressed(String deviceId) async {
  //   changeIsLoading(true);
  //   final result = await _returnBorrowedDeviceUseCase(deviceId);
  //   changeIsLoading(false);

  //   return result;
  // }

  void dispose() {
    _isLoading?.close();
  }
}
