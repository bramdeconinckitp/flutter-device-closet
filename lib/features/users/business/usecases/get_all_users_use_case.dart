import 'package:flutter_device_closet/features/users/business/entities/user.dart';
import 'package:flutter_device_closet/features/users/business/repositories/user_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@lazySingleton
class GetAllUsersUseCase {
  final UserRepository _userRepository;

  GetAllUsersUseCase(this._userRepository);

  Observable<List<User>> call() {
    return _userRepository.getAllUsers();
  }
}
