import 'package:flutter_device_closet/features/users/business/entities/user.dart';
import 'package:rxdart/rxdart.dart';

abstract class UserRepository {
  Observable<List<User>> getAllUsers();
}
