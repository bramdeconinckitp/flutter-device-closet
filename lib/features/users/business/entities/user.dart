import 'package:flutter/foundation.dart';

class User {
  final int id;
  final String name;

  User({
    @required this.id,
    @required this.name,
  });
}
