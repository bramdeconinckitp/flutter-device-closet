import 'dart:async';

import 'package:flutter_device_closet/core/services/navigation/service/navigation_service.dart';
import 'package:flutter_device_closet/features/users/business/entities/user.dart';
import 'package:flutter_device_closet/core/extensions/string_extensions.dart';
import 'package:flutter_device_closet/features/users/business/usecases/get_all_users_use_case.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

// TODO: should use @injectable when users are cached.
@singleton
class UserBloc {
  final GetAllUsersUseCase _getAllUsersUseCase;
  final NavigationService _navigationService;

  final _filteredUsers = BehaviorSubject<List<User>>();
  StreamSubscription<List<User>> _filteredUsersSubscription;
  Observable<List<User>> get filteredUsers => _filteredUsers.stream;
  Function(List<User>) get changeFilteredUsers => _filteredUsers.sink.add;

  final _nameFilter = BehaviorSubject<String>.seeded(null);
  String get currentNameFilterValue => _nameFilter.value;
  Observable<String> get nameFilter => _nameFilter.stream;
  Function(String) get changeNameFilter => _nameFilter.sink.add;

  UserBloc(
    this._getAllUsersUseCase,
    this._navigationService,
  ) {
    _getFilteredUsers();
  }

  void _getFilteredUsers() {
    _filteredUsersSubscription =
        Observable.combineLatest2(_getAllUsersUseCase(), nameFilter, (List<User> users, String name) {
      final filteredUsers = [...users];
      name = name?.toLowerCase();

      if (!name.isNullOrEmpty) {
        filteredUsers.removeWhere((user) => !user.name.toLowerCase().contains(name));
      }

      return filteredUsers;
    }).listen(changeFilteredUsers);
  }

  void onUserTapped(User user) {
    _navigationService.navigateBack(user);
    onClearTapped();
  }

  void onClearTapped() {
    changeNameFilter(null);
  }

  void dispose() {
    _filteredUsers.close();
    _filteredUsersSubscription?.cancel();
    _nameFilter.close();
  }
}
