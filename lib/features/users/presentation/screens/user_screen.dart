import 'package:flutter/material.dart';
import 'package:flutter_device_closet/core/widgets/general/basic_app_bar.dart';
import 'package:flutter_device_closet/core/widgets/general/basic_empty_state.dart';
import 'package:flutter_device_closet/core/widgets/general/loading_indicator.dart';
import 'package:flutter_device_closet/features/users/business/entities/user.dart';
import 'package:flutter_device_closet/features/users/presentation/blocs/user_bloc.dart';
import 'package:flutter_device_closet/features/users/presentation/screens/widgets/user_list_item.dart';
import 'package:provider/provider.dart';

import 'widgets/user_list_search_bar.dart';

class UserScreen extends StatelessWidget {
  const UserScreen();

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: BasicAppBar('Select a user'),
      body: UserListBody(),
    );
  }
}

class UserListBody extends StatelessWidget {
  const UserListBody();

  @override
  Widget build(BuildContext context) {
    final userBloc = Provider.of<UserBloc>(context);

    return StreamBuilder<List<User>>(
      stream: userBloc.filteredUsers,
      initialData: null,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const LoadingIndicator();
        }

        return Stack(
          children: <Widget>[
            if (snapshot.data.isEmpty)
              const BasicEmptyState(text: 'No users available')
            else
              UserList(users: snapshot.data),
            const UserListSearchBar(),
          ],
        );
      },
    );
  }
}

class UserList extends StatelessWidget {
  static const _paddingTop = kToolbarHeight + 4;

  final List<User> users;

  const UserList({@required this.users});

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.only(top: _paddingTop),
      children: ListTile.divideTiles(
        context: context,
        tiles: users.map((user) => UserListItem(user: user)),
      ).toList(),
    );
  }
}
