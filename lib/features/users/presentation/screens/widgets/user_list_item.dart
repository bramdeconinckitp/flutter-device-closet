import 'package:flutter/material.dart';
import 'package:flutter_device_closet/features/users/business/entities/user.dart';
import 'package:flutter_device_closet/features/users/presentation/blocs/user_bloc.dart';
import 'package:provider/provider.dart';

class UserListItem extends StatelessWidget {
  final User user;

  const UserListItem({@required this.user});

  @override
  Widget build(BuildContext context) {
    final userBloc = Provider.of<UserBloc>(context);

    return ListTile(
      leading: const Icon(Icons.account_circle),
      title: Text(user.name),
      onTap: () => userBloc.onUserTapped(user),
    );
  }
}