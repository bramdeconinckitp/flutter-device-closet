import 'package:flutter/material.dart';
import 'package:flutter_device_closet/features/users/presentation/blocs/user_bloc.dart';
import 'package:provider/provider.dart';

class UserListSearchBar extends StatelessWidget {
  const UserListSearchBar();

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.all(12),
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(32),
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(32),
        ),
        child: Row(
          children: const <Widget>[
            Expanded(
              child: _SearchBarTextField(),
            ),
            _SearchBarClearButton(),
          ],
        ),
      ),
    );
  }
}

class _SearchBarTextField extends StatefulWidget {
  const _SearchBarTextField();

  @override
  _SearchBarTextFieldState createState() => _SearchBarTextFieldState();
}

class _SearchBarTextFieldState extends State<_SearchBarTextField> {
  TextEditingController _searchBarController;
  UserBloc _userBloc;

  @override
  void initState() {
    super.initState();
    _searchBarController = TextEditingController();
  }

  @override
  void dispose() {
    _searchBarController?.dispose();
    super.dispose();
  }

  void _resetControllerIfNecessary() {
    if (_userBloc.currentNameFilterValue == null) {
      _searchBarController.text = '';
    }
  }

  @override
  Widget build(BuildContext context) {
    _userBloc = Provider.of<UserBloc>(context);

    return StreamBuilder<String>(
      stream: _userBloc.nameFilter,
      builder: (context, snapshot) {
        _resetControllerIfNecessary();

        return TextField(
          maxLines: 1,
          controller: _searchBarController,
          onChanged: _userBloc.changeNameFilter,
          decoration: const InputDecoration(
            hintText: 'Filter by name...',
            border: InputBorder.none,
            icon: Icon(
              Icons.search,
              color: Colors.black87,
            ),
          ),
        );
      },
    );
  }
}

class _SearchBarClearButton extends StatelessWidget {
  const _SearchBarClearButton();

  @override
  Widget build(BuildContext context) {
    final userBloc = Provider.of<UserBloc>(context);

    return IconButton(
      icon: const Icon(
        Icons.clear,
        color: Colors.black87,
      ),
      tooltip: 'Clear field',
      onPressed: userBloc.onClearTapped,
    );
  }
}
