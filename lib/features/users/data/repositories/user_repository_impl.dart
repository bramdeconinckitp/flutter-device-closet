import 'package:flutter_device_closet/core/api/device_closet_api.dart';
import 'package:flutter_device_closet/features/users/business/entities/user.dart';
import 'package:flutter_device_closet/features/users/business/repositories/user_repository.dart';
import 'package:flutter_device_closet/features/users/data/mappers/user_model_to_user_mapper.dart';
import 'package:flutter_device_closet/features/users/data/models/user_model.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@RegisterAs(UserRepository)
@lazySingleton
class UserRepositoryImpl implements UserRepository {
  final DeviceClosetApi _deviceClosetApi;
  final UserModelToUserMapper _userModelToUserMapper;

  UserRepositoryImpl(
    this._deviceClosetApi,
    this._userModelToUserMapper,
  );

  @override
  Observable<List<User>> getAllUsers() {
    return Observable.fromFuture(_getUsersFromApi()).map(_mapUserModelsToUsers);
  }

  Future<List<UserModel>> _getUsersFromApi() async {
    return _deviceClosetApi.getUsers().then((res) => res.body);
  }

  List<User> _mapUserModelsToUsers(List<UserModel> userModels) {
    return userModels.map(_mapUserModelToUser).toList();
  }

  User _mapUserModelToUser(UserModel userModel) {
    return _userModelToUserMapper.to(userModel);
  }
}
