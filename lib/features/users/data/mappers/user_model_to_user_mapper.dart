import 'package:flutter_device_closet/features/users/business/entities/user.dart';
import 'package:flutter_device_closet/features/users/data/models/user_model.dart';
import 'package:html/dom.dart' as html_parser;
import 'package:injectable/injectable.dart';

@lazySingleton
class UserModelToUserMapper {
  User to(UserModel model) {
    return User(
      id: model.id,
      name: html_parser.DocumentFragment.html(model.name).text,
    );
  }
}
