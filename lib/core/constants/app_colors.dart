import 'dart:ui';

import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryTextColor = Colors.black87;
  static const Color secondaryTextColor = Colors.black54;

  static const Color primaryIconColor = Colors.black87;

  static const Color pureWhite = Color(0xFFFFFFFF);

  static const Color lightBlue = Color(0xFF6EBBD3);
  static const Color darkBlue = Color(0xFF211551);

  static const Color lightGrey = Color(0xFFE1E1E1);
  static const Color searchFieldGrey = Color(0xFFDCDCDC);
}
