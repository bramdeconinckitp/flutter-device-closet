class AppDimensions {
  // Padding
  static const double paddingXSmall = 2.0;
  static const double paddingSmall = 4.0;
  static const double paddingMedium = 8.0;
  static const double paddingLarge = 16.0;
  static const double paddingXLarge = 24.0;
  static const double paddingXXLarge = 32.0;

  // Font sizes
  static const double fontSizeTitleLoginScreen = 40.0;
  static const double fontSizeButton = 16.0;

  // Radiuses
  static const double borderRadiusButton = 16.0;
}
