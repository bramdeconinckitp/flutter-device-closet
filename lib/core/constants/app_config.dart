class AppConfig {
  // API
  static const String baseUrl = 'http://localhost:8888';

  // Firebase authentication
  static const String googleSignInDomain = 'inthepocket.mobi';
}
