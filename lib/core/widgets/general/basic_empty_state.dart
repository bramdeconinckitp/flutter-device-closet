import 'package:flutter/material.dart';

class BasicEmptyState extends StatelessWidget {
  final String text;

  const BasicEmptyState({@required this.text});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(text ?? ''),
    );
  }
}
