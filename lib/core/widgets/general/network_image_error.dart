import 'package:flutter/material.dart';
import 'package:flutter_device_closet/core/constants/app_colors.dart';

class NetworkImageError extends StatelessWidget {
  const NetworkImageError();

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Icon(
        Icons.error,
        color: AppColors.primaryIconColor,
      ),
    );
  }
}
