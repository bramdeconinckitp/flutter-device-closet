import 'package:flutter/material.dart';
import 'package:flutter_device_closet/core/constants/app_dimensions.dart';

class UnknownRouteScreen extends StatelessWidget {
  final String routeName;

  const UnknownRouteScreen({@required this.routeName});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(AppDimensions.paddingXLarge),
        child: Center(
          child: Text(
            'No path for $routeName',
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
