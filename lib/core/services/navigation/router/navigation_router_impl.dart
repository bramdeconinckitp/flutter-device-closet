import 'package:flutter/material.dart';
import 'package:flutter_device_closet/core/services/navigation/router/navigation_router.dart';
import 'package:flutter_device_closet/core/services/navigation/router/navigation_routes.dart';
import 'package:flutter_device_closet/features/devices/business/entities/device.dart';
import 'package:injectable/injectable.dart';

@RegisterAs(NavigationRouter)
@lazySingleton
class NavigationRouterImpl with NavigationRoutes implements NavigationRouter {
  @override
  Route generateRoute(RouteSettings routeSettings) {
    if (routeSettings.name.contains(NavigationRouter.deviceDetail)) {
      final deviceId = (routeSettings.arguments as Device).id;
      return deviceDetailRoute(routeSettings, deviceId);
    }

    switch (routeSettings.name) {
      case NavigationRouter.devices:
        return devicesListRoute(routeSettings);

      case NavigationRouter.users:
        return usersListRoute(routeSettings);

      default:
        return unknownRoute(routeSettings);
    }
  }
}
