import 'package:flutter/material.dart';
import 'package:flutter_device_closet/core/widgets/screens/unknown_route_screen.dart';
import 'package:flutter_device_closet/features/devices/presentation/blocs/device_bloc.dart';
import 'package:flutter_device_closet/features/devices/presentation/blocs/device_detail_bloc.dart';
import 'package:flutter_device_closet/features/devices/presentation/screens/device_detail/device_detail_screen.dart';
import 'package:flutter_device_closet/features/devices/presentation/screens/devices/devices_screen.dart';
import 'package:flutter_device_closet/features/users/presentation/blocs/user_bloc.dart';
import 'package:flutter_device_closet/features/users/presentation/screens/user_screen.dart';
import 'package:provider/provider.dart';

import '../../injection.dart';

mixin NavigationRoutes {
  MaterialPageRoute devicesListRoute(RouteSettings routeSettings) {
    return MaterialPageRoute<dynamic>(
      settings: routeSettings,
      builder: (_) {
        return Provider(
          create: (_) => getIt<DeviceBloc>(),
          child: DevicesScreen(),
        );
      },
    );
  }

  MaterialPageRoute deviceDetailRoute(RouteSettings routeSettings, int deviceId) {
    return MaterialPageRoute<dynamic>(
      settings: routeSettings,
      builder: (_) {
        return Provider(
          create: (_) => getIt<DeviceDetailBloc>(),
          child: DeviceDetailScreen(deviceId: deviceId),
        );
      },
    );
  }

  MaterialPageRoute usersListRoute(RouteSettings routeSettings) {
    return MaterialPageRoute<dynamic>(
      settings: routeSettings,
      fullscreenDialog: true,
      builder: (_) {
        return Provider(
          create: (_) => getIt<UserBloc>(),
          child: const UserScreen(),
        );
      },
    );
  }

  MaterialPageRoute unknownRoute(RouteSettings routeSettings) {
    return MaterialPageRoute<dynamic>(
      settings: routeSettings,
      builder: (_) {
        return UnknownRouteScreen(routeName: routeSettings.name);
      },
    );
  }
}
