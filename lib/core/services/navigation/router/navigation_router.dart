import 'package:flutter/material.dart';

abstract class NavigationRouter {
  static const String devices = '/';
  static const String deviceDetail = '/device';
  static const String users = '/users';

  static const String deviceDetailParameter = 'deviceId';

  static String getDeviceDetailRoute(int deviceId) {
    return '${NavigationRouter.deviceDetail}?${NavigationRouter.deviceDetailParameter}=$deviceId';
  }

  Route<dynamic> generateRoute(RouteSettings routeSettings);
}
