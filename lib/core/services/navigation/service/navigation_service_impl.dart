import 'package:flutter/widgets.dart';
import 'package:injectable/injectable.dart';

import 'navigation_service.dart';

@RegisterAs(NavigationService)
@lazySingleton
class NavigationServiceImpl implements NavigationService {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();
  @override
  GlobalKey<NavigatorState> get navigatorKey => _navigatorKey;

  @override
  Future<dynamic> navigateTo(String routeName, [dynamic parameters]) {
    return _navigatorKey.currentState.pushNamed(routeName, arguments: parameters);
  }

  @override
  Future<dynamic> popBackstackAndNavigateTo(String routeName) {
    return _navigatorKey.currentState.pushNamedAndRemoveUntil(routeName, (Route<dynamic> route) => false);
  }

  @override
  void navigateBack([dynamic result]) {
    navigatorKey.currentState.pop<dynamic>(result);
  }
}