import 'package:flutter/widgets.dart';

abstract class NavigationService {
  GlobalKey<NavigatorState> get navigatorKey;
  Future<dynamic> navigateTo(String routeName, [dynamic arguments]);
  Future<dynamic> popBackstackAndNavigateTo(String routeName);
  void navigateBack([dynamic result]);
}
