class AlertResponse {
  final bool didConfirm;

  AlertResponse({
    this.didConfirm,
  });
}
