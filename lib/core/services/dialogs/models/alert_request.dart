import 'package:flutter/foundation.dart';

class AlertRequest {
  final String title;
  final String description;
  final String primaryButtonTitle;
  final String secondaryButtonTitle;

  AlertRequest({
    @required this.title,
    @required this.description,
    @required this.primaryButtonTitle,
    this.secondaryButtonTitle,
  });
}
