import 'package:flutter/material.dart';
import 'package:flutter_device_closet/core/constants/app_colors.dart';
import 'package:flutter_device_closet/core/constants/app_dimensions.dart';
import 'package:flutter_device_closet/core/extensions/string_extensions.dart';
import 'package:flutter_device_closet/core/services/injection.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'dialog_service.dart';
import 'models/alert_request.dart';
import 'models/alert_response.dart';

class DialogManager extends StatefulWidget {
  final Widget child;

  const DialogManager({@required this.child});

  @override
  _DialogManagerState createState() => _DialogManagerState();
}

class _DialogManagerState extends State<DialogManager> {
  final DialogService _dialogService = getIt<DialogService>();

  @override
  void initState() {
    super.initState();
    _dialogService.registerDialogListener(_presentAlert);
  }

  void _onConfirmPressed() {
    _dialogService.onCompleteDialog(AlertResponse(didConfirm: true));
    Navigator.of(context).pop();
  }

  void _onCancelRequested() {
    _dialogService.onCompleteDialog(AlertResponse(didConfirm: false));
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  void _presentAlert(AlertRequest request) {
    Alert(
      context: context,
      title: request.title,
      desc: request.description,
      closeFunction: _onCancelRequested,
      buttons: <DialogButton>[
        _buildAlertButton(request.primaryButtonTitle, _onConfirmPressed),
        if (!request.secondaryButtonTitle.isNullOrEmpty)
          _buildAlertButton(request.secondaryButtonTitle, _onCancelRequested),
      ],
    ).show();
  }

  DialogButton _buildAlertButton(String buttonTitle, VoidCallback onPressed) {
    return DialogButton(
      onPressed: onPressed,
      child: Text(
        buttonTitle,
        style: const TextStyle(
          color: AppColors.pureWhite,
          fontSize: AppDimensions.fontSizeButton,
        ),
      ),
    );
  }
}
