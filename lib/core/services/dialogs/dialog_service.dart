import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import 'models/alert_request.dart';
import 'models/alert_response.dart';

@lazySingleton
class DialogService {
  Function(AlertRequest) _showDialogListener;
  Completer<AlertResponse> _dialogCompleter;

  void registerDialogListener(Function(AlertRequest) showDialogListener) {
    _showDialogListener = showDialogListener;
  }

    void onCompleteDialog(AlertResponse response) {
    _dialogCompleter.complete(response);
    _dialogCompleter = null;
  }

  Future<AlertResponse> presentDialog({
    @required String title,
    @required String description,
    @required String primaryButtonTitle,
    String secondaryButtonTitle,
  }) {
    _dialogCompleter = Completer<AlertResponse>();

    _showDialogListener(
      AlertRequest(
        title: title,
        description: description,
        primaryButtonTitle: primaryButtonTitle,
        secondaryButtonTitle: secondaryButtonTitle,
      ),
    );

    return _dialogCompleter.future;
  }
}
