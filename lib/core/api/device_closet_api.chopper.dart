// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_closet_api.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$DeviceClosetApi extends DeviceClosetApi {
  _$DeviceClosetApi([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = DeviceClosetApi;

  @override
  Future<Response<List<DeviceModel>>> getDevices() {
    final $url = '/api/devices';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<DeviceModel>, DeviceModel>($request);
  }

  @override
  Future<Response<DeviceModel>> getDeviceById(String deviceId) {
    final $url = '/api/devices/$deviceId';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<DeviceModel, DeviceModel>($request);
  }

  @override
  Future<Response<CheckDeviceInOutResponseModel>> checkDeviceOut(
      int deviceId, Map<String, Object> body) {
    final $url = '/api/devices/$deviceId/checkout';
    final $body = body;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<CheckDeviceInOutResponseModel,
        CheckDeviceInOutResponseModel>($request);
  }

  @override
  Future<Response<CheckDeviceInOutResponseModel>> checkDeviceIn(int deviceId) {
    final $url = '/api/devices/$deviceId/checkin';
    final $request = Request('POST', $url, client.baseUrl);
    return client.send<CheckDeviceInOutResponseModel,
        CheckDeviceInOutResponseModel>($request);
  }

  @override
  Future<Response<List<UserModel>>> getUsers() {
    final $url = '/api/users';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<UserModel>, UserModel>($request);
  }
}
