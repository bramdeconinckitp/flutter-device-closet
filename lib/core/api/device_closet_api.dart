import 'package:chopper/chopper.dart';
import 'package:flutter_device_closet/features/devices/data/models/check_device_in_out_response_model.dart';
import 'package:flutter_device_closet/features/devices/data/models/device_model.dart';
import 'package:flutter_device_closet/features/users/data/models/user_model.dart';
import 'package:injectable/injectable.dart';

import 'device_closet_api_factory.dart';

part 'device_closet_api.chopper.dart';

@lazySingleton
@ChopperApi()
abstract class DeviceClosetApi extends ChopperService with DeviceClosetApiFactory {
  @factoryMethod
  static DeviceClosetApi create() => _$DeviceClosetApi(DeviceClosetApiFactory.createClient());

  @Get(path: '/api/devices')
  Future<Response<List<DeviceModel>>> getDevices();

  @Get(path: '/api/devices/{device_id}')
  Future<Response<DeviceModel>> getDeviceById(
    @Path('device_id') String deviceId,
  );

  @Post(path: '/api/devices/{device_id}/checkout')
  Future<Response<CheckDeviceInOutResponseModel>> checkDeviceOut(
    @Path('device_id') int deviceId,
    @Body() Map<String, Object> body,
  );

  @Post(path: '/api/devices/{device_id}/checkin')
  Future<Response<CheckDeviceInOutResponseModel>> checkDeviceIn(
    @Path('device_id') int deviceId,
  );

  @Get(path: '/api/users')
  Future<Response<List<UserModel>>> getUsers();
}
