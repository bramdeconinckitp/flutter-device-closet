import 'package:chopper/chopper.dart';
import 'package:flutter_device_closet/core/constants/app_config.dart';
import 'package:flutter_device_closet/features/devices/data/models/category_model.dart';
import 'package:flutter_device_closet/features/devices/data/models/check_device_in_out_response_model.dart';
import 'package:flutter_device_closet/features/devices/data/models/device_model.dart';
import 'package:flutter_device_closet/features/devices/data/models/manufacturer_model.dart';
import 'package:flutter_device_closet/features/devices/data/models/model_model.dart';
import 'package:flutter_device_closet/features/devices/data/models/rtd_location_model.dart';
import 'package:flutter_device_closet/features/users/data/models/user_model.dart';

import 'json_to_type_converter.dart';

mixin DeviceClosetApiFactory {
  static ChopperClient createClient() {
    return ChopperClient(
      baseUrl: AppConfig.baseUrl,
      converter: JsonToTypeConverter({
        CategoryModel: (Map<String, Object> json) => CategoryModel.fromJson(json),
        DeviceModel: (Map<String, Object> json) => DeviceModel.fromJson(json),
        ManufacturerModel: (Map<String, Object> json) => ManufacturerModel.fromJson(json),
        ModelModel: (Map<String, Object> json) => ModelModel.fromJson(json),
        RtdLocationModel: (Map<String, Object> json) => RtdLocationModel.fromJson(json),
        UserModel: (Map<String, Object> json) => UserModel.fromJson(json),
        CheckDeviceInOutResponseModel: (Map<String, Object> json) => CheckDeviceInOutResponseModel.fromJson(json),
      }),
    );
  }
}
