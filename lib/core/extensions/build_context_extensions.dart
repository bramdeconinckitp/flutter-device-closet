import 'package:flutter/widgets.dart';

extension BuildContextExtensions on BuildContext {
  void hideKeyboard() {
    FocusScope.of(this).unfocus();
  }
}
