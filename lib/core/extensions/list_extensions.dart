extension ListExtensions on List {
  bool get isNullOrEmpty {
    return this == null || isEmpty;
  }
}
